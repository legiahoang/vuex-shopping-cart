import Vue from "vue";
import App from "./App";
import store from "./store/index";
import { currency } from "./currency";
Vue.config.productionTip = false;

/* eslint-disable no-new */
// register filter globally
Vue.filter('currency', currency);
new Vue({
  el: "#app",
  store: store, // or use short hand which is just 'store'
  render: h => h(App)
});
