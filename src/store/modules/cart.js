import shop from "../../api/shop";

export default {
  namespaced: true,
  state: {
    items: [],
    checkoutStatus: null
  },
  getters: {
    cartProducts(state, getters, rootState, rootGetters) {
      return state.items.map(cartItem => {
        const product = rootState.products.items.find(
          product => product.id === cartItem.id
        );
        return {
          title: product.title,
          price: product.price,
          quantity: cartItem.quantity
        };
      });
    },
    cartTotal(state, getters) {
      // let total = 0;
      // getters.cartProducts.forEach(product => {
      //   total += product.price * product.quantity;
      // });
      // return total;
      return getters.cartProducts.reduce(
        (total, product) => total + product.price * product.quantity,
        0
      );
    }
  },
  actions: {
    addProductToCart(
      { state, getters, commit, rootState, rootGetters },
      product
    ) {
      if (rootGetters["products/productIsInStock"](product)) {
        // find cartItem
        const cartItem = state.items.find(item => product.id === item.id);
        if (!cartItem) {
          commit("pushProductToCart", product.id);
        } else {
          commit("incrementItemQuantity", cartItem);
        }
        //after adding to cart let reduce inventory number.
        commit("products/decrementProductInventory", product, { root: true });
      }
    },
    checkout({ state, commit }) {
      shop.buyProducts(
        state.items,
        () => {
          /**
           * when success, clear the cart and display a message to the user.
           */
          commit("emptyCart");
          commit("setCheckoutStatus", "success");
        },
        () => {
          //we can use es6 argument destructuring to grab only properties we need from the context object
          commit("setCheckoutStatus", "fail");
        }
      );
    }
  },
  mutations: {
    pushProductToCart(state, productId) {
      state.items.push({
        id: productId,
        quantity: 1
      });
    },
    incrementItemQuantity(state, cartItem) {
      cartItem.quantity++;
    },
    emptyCart(state) {
      state.items = [];
    },
    setCheckoutStatus(state, message) {
      message ? (state.checkoutStatus = message) : (state.checkoutStatus = "");
    }
  }
};
