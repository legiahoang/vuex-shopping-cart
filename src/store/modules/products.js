import shop from "../../api/shop";

export default {
  namespaced: true,
  state: {
    items: [] //available to all company
  },
  getters: {
    availableProducts(state, getters) {
      return state.items.filter(product => product.inventory > 0);
    },
    productIsInStock() {
      return product => {
        return product.inventory > 0;
      };
    }
  },
  actions: {
    fetchProducts(context) {
      return new Promise((resolve, reject) => {
        // would perform the API call to fetch the products
        shop.getProducts(products => {
          context.commit("setProducts", products);
          resolve();
        });
      });
    }
  },
  mutations: {
    setProducts(state, products) {
      // similar to (state, payload). Now, payload is products
      // update product
      state.items = products;
    },
    decrementProductInventory(state, product) {
      product.inventory--;
    }
  }
};
