import Vuex from "vuex";
import Vue from "vue";
import actions from "./action";
import cart from "./modules/cart";
import products from "./modules/products";
Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    cart,
    products
  },
  //for better understanding let compare it with Vue instance
  state: {
    // === data (in Vue instance)
    //list object which have {id, quantity}
  },
  getters: {
    // === computed property
    productsCount() {
      // length of product array
    }
    /**
     *  getters track their own dependencies and automatically update when dependency changes.
     **/
  },
  /**
   * Action can be asynchronous
   */
  actions, // i am using shorthand
  /**
   * Mutations are responsible for setting and updating the state.
   * Mutations have to be asynchronous
   */
  mutations: {
    // something new
    /*
     if we want to set the products array, we have to call a mutation,
     for example setProducts
      */
  }

  /**
   * it is really important to understand the different responsibilities
   * of MUTATIONS and ACTIONS.
   *
   * e.g: the fetchProducts action will be responsible for making the AJAX call
   * and setProducts mutation will be responsible for updating the state and setting
   * the products array.
   * It might be tempting(attractive) to set the products directly in the fetProducts action,
   * But this goes against the whole idea of a centralized store.
   *
   * What we should do instead: is to call mutation that sets the products
   * Mutation should be as simple as possible, and only responsible for updating just a piece of the
   * state. On the other hand, actions can be complex but never update the state.
   * If you follow this pattern it is very likely to have less bugs in your code.
   *
   *
   * Vuex will automatically pass the state as the first parameter in every mutation.
   * We can pass an additional parameter when calling a mutation, that will be the payload.
   * in our example, the payload is the products.
   * We can then update the state, by setting state.products to payload's products. line 34
   *
   * KEEP IN MIND: the purpose of mutations is not to fetch data or make complex calculations
   * but to alter state
   *
   */
});
